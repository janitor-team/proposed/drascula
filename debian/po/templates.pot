# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: Drascula 1.0\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2013-02-15 21:18+0100\n"
"PO-Revision-Date: 2013-02-15 21:25+0100\n"
"Last-Translator: Markus Koschany <apo@gambaru.de>\n"
"Language-Team: Debian Games Team <pkg-games-devel@lists.alioth.debian.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: debian/bin/drascula:13
msgid "Usage: drascula [OPTION]"
msgstr ""

#: debian/bin/drascula:14
msgid "Start Drascula: The Vampire Strikes Back"
msgstr ""

#: debian/bin/drascula:16
msgid "--help        display this help and exit"
msgstr ""

#: debian/bin/drascula:17
msgid "--version     display version information and exit"
msgstr ""

#: debian/bin/drascula:18
msgid "--de          play the German version of Drascula"
msgstr ""

#: debian/bin/drascula:19
msgid "--fr          play the French version of Drascula"
msgstr ""

#: debian/bin/drascula:20
msgid "--it          play the Italian version of Drascula"
msgstr ""

#: debian/bin/drascula:21
msgid "--es          play the Spanish version of Drascula"
msgstr ""

#: debian/bin/drascula:26
msgid "Drascula: The Vampire Strikes Back 1.0"
msgstr ""

#: debian/bin/drascula:27
msgid "Copyright (C) Alcachofa Soft S.L"
msgstr ""

#: debian/bin/drascula:28
msgid "ScummVM engine Copyright (C) The ScummVM Team"
msgstr ""

#: debian/bin/drascula:49
msgid "Please install the drascula-german package."
msgstr ""

#: debian/bin/drascula:58
msgid "Please install the drascula-french package."
msgstr ""

#: debian/bin/drascula:66
msgid "Please install the drascula-italian package."
msgstr ""

#: debian/bin/drascula:74
msgid "Please install the drascula-spanish package."
msgstr ""

